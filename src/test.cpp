#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SOIL/SOIL.h>

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <chrono>

#include "vertices.h"
#include "callbacks.h"

#define SCREEN_WIDTH    800
#define SCREEN_HEIGHT   800

int main()
{
    GLFWwindow * window;
    GLuint vbo, vso, fso, vao, ebo, tex;
    GLint status;

    if (!glfwInit())
        return -1;

    // require opengl context to support version >=3.2
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    // only support new core functionality
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // width, height, 
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "OpenGL", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    glewInit();

    // vaos store all links between attribs and vbos with raw vertex data
    // use when using different shaders and vertex layouts, and changing the active
    //  shader program
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // repeat texture and don't filter
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    int width, height;
    unsigned char * image =
        SOIL_load_image("img/blobsteam.png", &width, &height, 0, SOIL_LOAD_RGB);
    // params: texture target, LOD, pix format, (width, height), must be 0 (border),
    //  format of pixels, pixels themselves
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
            GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);

    // elements - unsigned ints referring to vertices bound to GL_ARRAY_BUFFER
    GLuint elements[] = {
        0, 1, 2,
        0, 1, 6,
        0, 6, 2,
        2, 8, 5,
        2, 4, 5,
        8, 4, 5,
        7, 3, 4,
        1, 3, 4,
        1, 3, 7,
        6, 7, 9,
        9, 7, 8,
        6, 9, 8
    };
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

    // allows uploading vertex data to graphics card
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo); // make vbo the active buffer object
    // static draw: vertex data uploaded once and drawn many times
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // create shader object and load code onto it
    vso = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vso, 1, &vertex_shader, NULL);
    glCompileShader(vso);

    // check if shader compiled successfully
    glGetShaderiv(vso, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) return -1;

    fso = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fso, 1, &fragment_shader, NULL);
    glCompileShader(fso);

    glGetShaderiv(fso, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) return -1;

    // program to combine two shaders together
    GLuint shader_program = glCreateProgram();
    glAttachShader(shader_program, vso);
    glAttachShader(shader_program, fso);
    glBindFragDataLocation(shader_program, 0, "outColor");
    glLinkProgram(shader_program);
    glUseProgram(shader_program);

    // reference to position input in vertex shader
    GLint pos_attr = glGetAttribLocation(shader_program, "position");
    // 2nd param: number of values for input, or # of components of vec
    // 5th param: stride - how many bytes between each position attribute in array
    // 6th param: offset - how many bytes from start of array the attribute occurs
    glEnableVertexAttribArray(pos_attr);
    glVertexAttribPointer(pos_attr, 3, GL_FLOAT, GL_FALSE,
            8*sizeof(GLfloat), 0); // 4th param: normalize

    GLint col_attr = glGetAttribLocation(shader_program, "color");
    glEnableVertexAttribArray(col_attr);
    glVertexAttribPointer(col_attr, 3, GL_FLOAT, GL_FALSE,
            8*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));

    GLint tex_attr = glGetAttribLocation(shader_program, "texcoord");
    glEnableVertexAttribArray(tex_attr);
    glVertexAttribPointer(tex_attr, 2, GL_FLOAT, GL_FALSE,
            8*sizeof(GLfloat), (void*)(6*sizeof(GLfloat)));

    glEnable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // 180 degree CCW transformation matrix
    glm::mat4 trans = glm::mat4(1.0f);
    // transformation matrix:
    // 2nd param: how many matrices to upload
    // 3rd param: if matrices should be transposed
    // 4th param: matrix to upload, turned into array of 16 floats first
    GLint uni_trans = glGetUniformLocation(shader_program, "model");
    glUniformMatrix4fv(uni_trans, 1, GL_FALSE, glm::value_ptr(trans));

    camera_height = 1.2f;
    glm::vec3 camera_pos = glm::vec3(1.2f, 1.2f, camera_height);
    glm::vec3 camera_center = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 camera_up = glm::vec3(0.0f, 0.0f, 1.0f);
    glm::mat4 view = glm::lookAt(camera_pos, camera_center, camera_up);
    GLint uni_view = glGetUniformLocation(shader_program, "view");
    glUniformMatrix4fv(uni_view, 1, GL_FALSE, glm::value_ptr(view));

    glm::mat4 proj = glm::perspective(glm::radians(45.0f), 1.0f, 1.0f, 10.0f);
    GLint uni_proj = glGetUniformLocation(shader_program, "proj");
    glUniformMatrix4fv(uni_proj, 1, GL_FALSE, glm::value_ptr(proj));

    // indicates how many frames to wait until swapping the buffers (vsync)
    glfwSwapInterval(1);

    glfwSetKeyCallback(window, key_callback);

    auto t_start = std::chrono::high_resolution_clock::now();
    auto t_old = t_start;
    // loop until window close
    while (!glfwWindowShouldClose(window)) {
        auto t_now = std::chrono::high_resolution_clock::now();
        float time = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start).count();
        float tick = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_old).count();
        t_old = t_now;
        float scale = (cosf(time* 4.0f) + 1.0f) / 2.0f;
        trans = glm::rotate(trans, tick * rotation_dir, glm::vec3(0.0f, 0.0f, 1.0f)); 
        //glm::mat4 scaled = glm::scale(trans, glm::vec3(scale, scale, 1.0f));
        //glUniformMatrix4fv(uni_trans, 1, GL_FALSE, glm::value_ptr(scaled));
        glUniformMatrix4fv(uni_trans, 1, GL_FALSE, glm::value_ptr(trans));
        camera_pos.z = camera_height;
        glm::mat4 view = glm::lookAt(camera_pos, camera_center, camera_up);
        glUniformMatrix4fv(uni_view, 1, GL_FALSE, glm::value_ptr(view));

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // uses element array to determine draw order
        // 2nd param: # of indices to draw
        // 3rd param: type of element data
        // 4th param: offset
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glDeleteProgram(shader_program);
    glDeleteShader(fso);
    glDeleteShader(vso);

    glDeleteBuffers(1, &vbo);

    glDeleteVertexArrays(1, &vao);

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
