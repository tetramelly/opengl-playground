#include <GLFW/glfw3.h>

int rotation_dir;
float camera_height;

void key_callback(GLFWwindow* window, int key,
        int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        else if (key == GLFW_KEY_RIGHT)
            rotation_dir = 1;
        else if (key == GLFW_KEY_LEFT)
            rotation_dir = -1;
        else if (key == GLFW_KEY_SPACE)
            rotation_dir = 0;
        else if (key == GLFW_KEY_UP)
            camera_height = camera_height >= 2.0f ? 2.0f : camera_height + 0.1f;
        else if (key == GLFW_KEY_DOWN)
            camera_height = camera_height <= 0.0f ? 0.0f : camera_height - 0.1f;
    }
}
