#ifndef CALLBACKS_H

extern int rotation_dir;
extern float camera_height;

void key_callback(GLFWwindow * window, int key, int scancode, int action, int mods);

#endif // CALLBACKS_H
