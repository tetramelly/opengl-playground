CC      := g++
CCFLAGS := -Iinclude
LDFLAGS := -lglfw -lGL -lGLEW -lSOIL

TARGETS := test
OBJ     := test.o vertices.o callbacks.o

.PHONY: all clean

all: $(TARGETS)

transformations: src/transformations.cpp
	$(CC) -o $@ $<

clean:
	rm $(TARGETS) $(OBJ)

$(OBJ): %.o : src/%.cpp
	$(CC) -c -o $@ $< $(CCFLAGS)

$(TARGETS): % : $(OBJ) %.o
	$(CC) -o $@ $^ $(CCFLAGS) $(LDFLAGS)
